(function () {
    'use strict';

    var global = tinymce.util.Tools.resolve('tinymce.PluginManager');

    function register(editor) {
        editor.on("click", e => {
            var el = e.target;
            let attr = el.getAttribute("filters");
            if (attr) {
                var len = Math.sqrt(Math.pow(e.offsetY, 2) + Math.pow(e.offsetX, 2));
                let start = false;
                if (el.tagName === "SPAN" && len < 5) start = true;
                if (el.tagName === "DIV" && len < 19) start = true;
                if (start) {
                    window.selectedNode = el.closest("[filters]");
                    editor.selection.select(window.selectedNode);
                    let obj = {};
                    for (let keyValue of attr.split(",")) {
                        let arr = keyValue.split(":");
                        let val = arr[1];
                        if (val === "true") val = true;
                        if (val === "false") val = false;
                        obj[arr[0]] = val;
                    }
                    openFunction(editor, obj);
                }
            }
        });
        var style = `[filters]{position:relative;padding:1px 2px;line-height:1;background:#8080802b;display:inline-block}[filters]:hover:after{cursor:pointer;content:"✎";border:none;text-align:center;width:22px;height:22px;background:#8c8c8c;line-height:22px;position:absolute;top:-10px;left:-4px;font-size:12px;color:white;border-radius:100%}span[filters]:hover:after{top:-13px;left:-13px}`;
        editor.contentStyles.push(style);
        editor.ui.registry.addButton("dateFilter", {
            text: "סינון תאריך",
            icon: "template",
            onAction: () => openFunction(editor)
        });
    }

    function openFunction(editor, initialData) {
        editor.windowManager.open({
            title: "הגבלת תאריך",
            initialData: initialData,
            body: {
                type: "panel",
                items: [
                    {
                        type: "label",
                        label: "תאריך התחלה",
                        items: [
                            {
                                columns: 2,
                                type: "grid",
                                items: [
                                    {
                                        type: "selectbox",
                                        name: "startMonth",
                                        label: "חודש",
                                        items: [
                                            {value: "0", text: "בחירת חודש"},
                                            {text: "תשרי", value: "7"},
                                            {text: "חשוון ", value: "8"},
                                            {text: "כסלו", value: "9"},
                                            {text: "טבת", value: "10"},
                                            {text: "שבט", value: "11"},
                                            {text: "אדר/אדר א", value: "12"},
                                            {text: "אדר ב", value: "13"},
                                            {text: "ניסן ", value: "1"},
                                            {text: "אייר", value: "2"},
                                            {text: "סיוון", value: "3"},
                                            {text: "תמוז ", value: "4"},
                                            {text: "אב", value: "5"},
                                            {text: "אלול", value: "6"}
                                        ]
                                    },
                                    {
                                        type: "selectbox",
                                        name: "startDay",
                                        label: "יום",
                                        items: [
                                            {value: "0", text: "בחירת יום"},
                                            {value: "1", text: "א"},
                                            {value: "2", text: "ב"},
                                            {value: "3", text: "ג"},
                                            {value: "4", text: "ד"},
                                            {value: "5", text: "ה"},
                                            {value: "6", text: "ו"},
                                            {value: "7", text: "ז"},
                                            {value: "8", text: "ח"},
                                            {value: "9", text: "ט"},
                                            {value: "10", text: "י"},
                                            {value: "11", text: "יא"},
                                            {value: "12", text: "יב"},
                                            {value: "13", text: "יג"},
                                            {value: "14", text: "יד"},
                                            {value: "15", text: "טו"},
                                            {value: "16", text: "טז"},
                                            {value: "17", text: "יז"},
                                            {value: "18", text: "יח"},
                                            {value: "19", text: "יט"},
                                            {value: "20", text: "כ"},
                                            {value: "21", text: "כא"},
                                            {value: "22", text: "כב"},
                                            {value: "23", text: "כג"},
                                            {value: "24", text: "כד"},
                                            {value: "25", text: "כה"},
                                            {value: "26", text: "כו"},
                                            {value: "27", text: "כז"},
                                            {value: "28", text: "כח"},
                                            {value: "29", text: "כט"},
                                            {value: "30", text: "ל"}
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        type: "label",
                        label: "תאריך סוף",
                        items: [
                            {
                                columns: 2,
                                type: "grid",
                                items: [
                                    {
                                        type: "selectbox",
                                        name: "endMonth",
                                        label: "חודש",
                                        items: [
                                            {value: "0", text: "בחירת חודש"},
                                            {text: "תשרי", value: "7"},
                                            {text: "חשוון ", value: "8"},
                                            {text: "כסלו", value: "9"},
                                            {text: "טבת", value: "10"},
                                            {text: "שבט ", value: "11"},
                                            {text: "אדר/אדר א", value: "12"},
                                            {text: "אדר ב", value: "13"},
                                            {text: "ניסן ", value: "1"},
                                            {text: "אייר", value: "2"},
                                            {text: "סיוון", value: "3"},
                                            {text: "תמוז ", value: "4"},
                                            {text: "אב", value: "5"},
                                            {text: "אלול", value: "6"}
                                        ]
                                    },
                                    {
                                        type: "selectbox",
                                        name: "endDay",
                                        label: "יום",
                                        items: [
                                            {value: "0", text: "בחירת יום"},
                                            {value: "1", text: "א"},
                                            {value: "2", text: "ב"},
                                            {value: "3", text: "ג"},
                                            {value: "4", text: "ד"},
                                            {value: "5", text: "ה"},
                                            {value: "6", text: "ו"},
                                            {value: "7", text: "ז"},
                                            {value: "8", text: "ח"},
                                            {value: "9", text: "ט"},
                                            {value: "10", text: "י"},
                                            {value: "11", text: "יא"},
                                            {value: "12", text: "יב"},
                                            {value: "13", text: "יג"},
                                            {value: "14", text: "יד"},
                                            {value: "15", text: "טו"},
                                            {value: "16", text: "טז"},
                                            {value: "17", text: "יז"},
                                            {value: "18", text: "יח"},
                                            {value: "19", text: "יט"},
                                            {value: "20", text: "כ"},
                                            {value: "21", text: "כא"},
                                            {value: "22", text: "כב"},
                                            {value: "23", text: "כג"},
                                            {value: "24", text: "כד"},
                                            {value: "25", text: "כה"},
                                            {value: "26", text: "כו"},
                                            {value: "27", text: "כז"},
                                            {value: "28", text: "כח"},
                                            {value: "29", text: "כט"},
                                            {value: "30", text: "ל"}
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        type: "label",
                        label: "יום בשבוע",
                        items: [
                            {
                                type: "selectbox",
                                name: "weekday",
                                items: [
                                    {value: "", text: "בחירה"},
                                    {value: "1", text: "ראשון"},
                                    {value: "2", text: "שני"},
                                    {value: "3", text: "שלישי"},
                                    {value: "4", text: "רביעי"},
                                    {value: "5", text: "חמישי"},
                                    {value: "6", text: "שישי"},
                                    {value: "7", text: "שבת"}
                                ]
                            }
                        ]
                    },
                    {
                        type: "label",
                        label: "דגל",
                        items: [
                            {
                                type: "selectbox",
                                name: "flag",
                                items: [
                                    {value: "", text: "בחירה"},
                                    {value: "male", text: "גבר"},
                                    {value: "female", text: "אישה"},
                                    {value: "avinuMalkenu", text: "אבינו מלכנו"},
                                    {value: "cholHamoedPesach", text: "חול המועד פסח"},
                                    {value: "cholHamoedSuccot", text: "חול המועד סוכות"}
                                ]
                            }
                        ]
                    },
                    {type: "checkbox", name: "roshChodesh", label: "ראש חודש"},
                    {type: "checkbox", name: "reference", label: "רפרנס"},
                    {type: "checkbox", name: "instruction", label: "הוראות"}
                ]
            },
            onClose: () => {
                window.selectedNode = null;
            },
            onAction: dialog => {
                if (window.selectedNode) {
                    let innerHTML = window.selectedNode.innerHTML;
                    window.selectedNode.remove();
                    editor.selection.setContent(innerHTML);
                }
                window.selectedNode = null;
                dialog.close();
            },
            onSubmit: dialog => {
                var data = dialog.getData();
                let dataString = JSON.stringify(data).replace(/[{"}]/gm, "");
                if (window.selectedNode) {
                    window.selectedNode.setAttribute("filters", dataString);
                } else {
                    let content = editor.selection.getContent({format: "html"});
                    let el = content.match(/<p/) ? "div" : "span";
                    editor.selection.setContent(
                        `<${el} filters="${dataString}">${content}</${el}>`
                    );
                }
                window.selectedNode = null;
                dialog.close();
            },
            buttons: [
                {type: "custom", name: "clean", text: "נקה"},
                {primary: true, type: "submit", name: "submit", text: "אישור"}
            ]
        });
    }

    function Plugin() {
        global.add('flagger', function (editor) {
            register(editor);
            return {};
        });
    }

    Plugin();
}());
